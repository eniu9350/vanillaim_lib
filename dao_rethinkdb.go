package lib

import (
	"fmt"
	r "github.com/dancannon/gorethink"
)

// ==prerequiste==
// run following command in shell before query:
// r.dbCreate(RTDB_DBNAME)
// r.db(RTDB_DBNAME).tableCreate(RTDB_TABLENAME_USER)

// ===implements==
// UserDao

const RTDB_DBNAME = "vnim"

const RTDB_TABLENAME_USER = "users"
const RTDB_TABLENAME_AUTOINC = "autoinc"

const RTDB_ATTR_VALUE_AUTOINC_INIT = int(5000000)

type CompositeDaoRethinkDB struct	{
	Session *r.Session
}

func NewCompositeDaoRethinkDB(session *r.Session) CompositeDaoRethinkDB	{
	compositeDaoRethinkDB := CompositeDaoRethinkDB{}
	compositeDaoRethinkDB.Session = session
	return compositeDaoRethinkDB
}

func (compositeDaoRethinkDB *CompositeDaoRethinkDB)DbopInitDb(){
	compositeDaoRethinkDB.DbopInitAutoIncValue("user") //not used yet
}

func (compositeDaoRethinkDB *CompositeDaoRethinkDB)DbopInitAutoIncValue(collName string) int {
	resp, err := r.DB(RTDB_DBNAME).Table(RTDB_TABLENAME_AUTOINC).GetAllByIndex("collname", collName).Run(compositeDaoRethinkDB.Session)
	var row AutoIncModel
	//TODO: no check of multiple rows?
	err = resp.One(&row)

	if err == nil{
		fmt.Println("[DbopInitAutoIncValue]Error: value existed!")
		return -1
	}	else if err == r.ErrEmptyResult {
		resp2, err := r.DB(RTDB_DBNAME).Table(RTDB_TABLENAME_AUTOINC).Insert(map[string]interface{}{
			"collname": collName,
			"currvalue": RTDB_ATTR_VALUE_AUTOINC_INIT,
		}).RunWrite(compositeDaoRethinkDB.Session)

		fmt.Printf("%d row inserted\n", resp2.Inserted)

		if err != nil {
			fmt.Print(err)
			return -1
		}

		return 0

	}else{
		fmt.Println("[DbopInitAutoIncValue]Unknown error!")
		return -1
	}

}

//make sure init function is called before this function
func (compositeDaoRethinkDB *CompositeDaoRethinkDB)DbopGetAutoIncValue(collName string) int { //TODO int or long?
	//inc
	resp, err := r.DB(RTDB_DBNAME).Table(RTDB_TABLENAME_AUTOINC).GetAllByIndex("collname", collName).Run(compositeDaoRethinkDB.Session)
	//TODO: study diff of run and runwrite(return value diff? other diffs?)

	var row AutoIncModel
	//TODO: no check of multiple rows?
	err = resp.One(&row)

	if err == nil {
		fmt.Println("error nil")

		respUpdate, errUpdate := r.DB(RTDB_DBNAME).Table(RTDB_TABLENAME_AUTOINC).Get(row.Id).Update(map[string]interface{}{
			"currvalue": r.Row.Field("currvalue").Add(1).Default(0),
		}, r.UpdateOpts{
			// ReturnChanges: "return_changes",
			ReturnChanges: true,
		}).Run(compositeDaoRethinkDB.Session) //or RunWrite?

		if errUpdate != nil {
			fmt.Print(errUpdate)
			return -1 //TODO: right?
		}

		// respUpdate.One(&row)
		//ref: https://github.com/dancannon/gorethink/issues/124
		//TODO: no check of err
		respUpdateRow := &r.WriteResponse{}
		respUpdate.One(respUpdateRow)
		newItem := respUpdateRow.Changes[0].NewValue

		//tmp
		newCurrValue := newItem.(map[string]interface {})
		return int(newCurrValue["currvalue"].(float64))	//may be (int) sometimes
		// fmt.Print(newCurrValue["currvalue"])
		// fmt.Printf("curent value=%d", row.CurrValue)

	} else if err == r.ErrEmptyResult {
		fmt.Print("[GetAutoIncValue]result not found!")
		return -1

	} else {
		fmt.Print(err)
		return -1
	}
}

func (compositeDaoRethinkDB *CompositeDaoRethinkDB)UserCreate(userName string, passWord string) int	{
	resp, err := r.DB(RTDB_DBNAME).Table(RTDB_TABLENAME_USER).Insert(UserModel{
		// ID:      1,
		Username: userName,
		Password: passWord,
	}).RunWrite(compositeDaoRethinkDB.Session)

	fmt.Printf("%d row inserted", resp.Inserted)

	if err != nil {
		fmt.Print(err)
		return -1
	}

	return 0
}
