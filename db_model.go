package lib

//rethinkdb models
type UserModel struct{
  Id      string    `gorethink:"id"`  //will set 0 as default if id attribute is specified here
  Username string `gorethink:"username"`
  Password string `gorethink:"password"`
}


//
type AutoIncModel struct  {
  Id string `gorethink:"id"` //id
  CollName string `gorethink:"colname"` //collection name
  CurrValue int `gorethink:"currvalue"` //current value
}
