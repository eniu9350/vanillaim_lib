package lib

import (
	// "fmt"
	"log"
	"bytes"
	// "net"
	// "os"
	// "strconv"
	// "realjin.info/vanilla_im/lib"
)

//to be extracted to module
// const (
// 	MSGHEADER_VERB_CHAT = iota
// 	MSGHEADER_VERB_LOGIN
// )

//HEADER VERB
const MSGHEADER_VERB_CHAT = "CHAT"
const MSGHEADER_VERB_LOGIN = "LOGIN"
const MSGHEADER_VERB_LOGOUT = "LOGOUT"
const MSGHEADER_VERB_REGISTER = "REGISTER"
const MSGHEADER_VERB_LOGIN_FIN = "LOGIN_FIN"
const MSGHEADER_VERB_LOGOUT_FIN = "LOGOUT_FIN"
const MSGHEADER_VERB_REGISTER_FIN = "REGISTER_FIN"

//BODY.PROP KEY
const MSGBODY_PROPS_BASIC_KEY_FROM = "from"
const MSGBODY_PROPS_BASIC_KEY_TO = "to"
const MSGBODY_PROPS_BASIC_KEY_REQUESTER = "requester"

const MSGBODY_PROPS_BASIC_KEY_PASSWORD = "password"
const MSGBODY_PROPS_BASIC_KEY_CONTENT = "content"

const MSGBODY_PROPS_BASIC_KEY_RESULT = "result"

//BODY.PROP VALUE(of "result" key)
const MSGBODY_PROPS_VALUE_OF_RESULT_OK = "ok"
const MSGBODY_PROPS_VALUE_OF_RESULT_ERROR_UNKNOWN = "error.unknown"
const MSGBODY_PROPS_VALUE_OF_RESULT_ERROR_LOGIN_WRONG_PASSWORD = "error.login.wrongpassword"
const MSGBODY_PROPS_VALUE_OF_RESULT_ERROR_LOGIN_USER_NOT_EXIST = "error.login.usernotexist"


//presence
// const PRESENCE_ONLINE = "ONLINE"
const PRESENCE_NOTOFFLINE_EXT_AVAILABLE = "AVAILABLE"
const PRESENCE_OFFLINE_OFFLINE = "OFFLINE"



type MsgHeader struct {
	Verb string
}

type MsgBody struct {
	Props map[string]string
}

type Msg struct {
	Header MsgHeader
	Body   MsgBody
}

func (msg Msg) Show() {
	log.Printf("header:\n verb is %s", msg.Header.Verb)
	log.Printf("body:")
	for k := range msg.Body.Props {
		log.Printf("%s = %s", k, msg.Body.Props[k])
	}
}

func (msg Msg) Fun1() string{
	return ""
}

func (msg Msg) ToString() string{
	var buffer bytes.Buffer

	// for i := 0; i < 1000; i++ {
		buffer.WriteString("[")
		buffer.WriteString(msg.Header.Verb)
		buffer.WriteString("]")
		buffer.WriteString("\r\n")
		for k := range msg.Body.Props {
			buffer.WriteString(k)
			buffer.WriteString(": ")
			buffer.WriteString(msg.Body.Props[k])
			buffer.WriteString("\r\n")
			// log.Printf("%s = %s", k, msg.Body.Props[k])
		}
		buffer.WriteString("\r\n")
		return buffer.String()
	// }

	// fmt.Println(buffer.String())
}

func (msg Msg) GetProp(name string) string{
	//TODO: check dup keys
	for k := range msg.Body.Props {
		if k==name	{
			return msg.Body.Props[k]
		}
	}
	return ""
}

func NewMsg() Msg {
	msg := Msg{}
	msg.Body = MsgBody{make(map[string]string)}
	msg.Header = MsgHeader{string("")}
	return msg
}
