package lib

import (
	"fmt"
	// r "github.com/dancannon/gorethink"
	redis "github.com/garyburd/redigo/redis"
)

const REDISDB_KEYPREFIX_PRESENCE = "p."

func PresenceUpdate(uid string, presence string) int {
	// conn, err := redis.Dial("tcp", "localhost:6379")
	// if err != nil {
	//   fmt.Printf("redis connection error")
	//   return -1
	//     // handle error
	// }
	//TODO: error not handled
	n, _ := conn.Do("SET", REDISDB_KEYPREFIX_PRESENCE+uid, presence)

	fmt.Printf("redis set op ret=%d", n)

	return 0
	// defer c.Close()
}

func DbopPresenceGet(conn redis.Conn, uid string) string {
	//TODO: error not handled
	fmt.Printf("key=%s\n", REDISDB_KEYPREFIX_PRESENCE+uid)
	//TODO: error not handled
	presence, _ := redis.String(conn.Do("GET", REDISDB_KEYPREFIX_PRESENCE+uid))

	return presence

	// defer c.Close()
}
