package lib

import (
	"strings"
)

type Pid struct {
	Uid string
	Pid string
}

func PidParseString(s string) Pid {
	//TODO: vanilla impl
	if strings.Index(s, ":") == -1 {
		return Pid{s, ""}
	} else {
		tokens := strings.Split(s, ":")
		return Pid{tokens[0], tokens[1]}
	}

}
